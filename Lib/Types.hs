module Types where

import Data.Text (Text)
import Data.HashMap.Lazy (HashMap)
import Data.Aeson
import Control.Applicative ((<$>))

{- CellDef record

The CellDef record holds Cell Abbreviation of CSS Class
rule.

We use JSON to store css definitions because it is
faster than YAML to read which matters because the file is very large,
and it is still easy for humans to edit.

-}


type CellDefMap = HashMap Text CellDef

type TextMap = HashMap Text Text

data CellDef = CellDef
    Text -- Template
    (Maybe TextMap) -- Aliases
  deriving (Eq, Show)

{- css.json

`css.json` actually holds a lot of information that we don't
need that is useful for other tools like the doc site.
Cell only uses the template and aliases fields.

-}

instance FromJSON CellDef where
  parseJSON (Object v) = CellDef <$>
    v .:  "template" <*>
    v .:? "aliases"
  parseJSON _ = error "Expected JSON of type Object."


{- Config record

The Config record holds data read from the
local config file, default name `cell.yaml`
manually specify a file with `-c`.

-}


data Config = Config
  { _breakpoints      :: Maybe TextMap
  , _scripts          :: Maybe Bool
  , _prefixProperties :: Maybe (HashMap Text [Text])
  , _prefixValues     :: Maybe (HashMap Text [Text])
  , _varibles         :: Maybe TextMap
  , _helperDefs       :: Maybe CellDefMap
  } deriving (Eq, Show)

instance FromJSON Config where
  parseJSON (Object v) = Config <$>
    v .:? "Breakpoints" <*>
    v .:? "ScanScripts" <*>
    v .:? "PrefixProperties" <*>
    v .:? "PrefixValues" <*>
    v .:? "Variables" <*>
    v .:? "Helpers"
  parseJSON _ = error "Error parsing config, expected YAML."


{- Cell Record

The Cell record type holds the state of Cell:
  CSS Definitions, Config, Runtime Options.

CSS Definitions:
  The global Cell definitions file is stored at
  `/usr/share/cell/css.json`.

Config:
  Local to project.
  `cell.yaml` is the default config file quarc will
  look for at runtime. e.g `index.html cell.yaml`
  manually specify a config file with `-c`.

The Config holds user defined helper classes,
varibles (for colors, common lengths, etc..),

-}


data Cell = Cell
  { _cellDefs        :: CellDefMap
  , _pseudoClassDefs :: TextMap
  , _config          :: Config
  } deriving (Eq, Show)

