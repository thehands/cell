{-# LANGUAGE TemplateHaskell #-}

module Resources (cssJson) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)

cssJson :: IO ByteString
cssJson = return $(embedFile "css.json")
