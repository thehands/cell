import Data.Text (Text)
import qualified Data.Text    as T
import qualified Data.Text.IO as T
import qualified Data.ByteString as BS (readFile)
import qualified Data.List as L
import Data.HashMap.Lazy (HashMap, (!))
import qualified Data.HashMap.Lazy as HM hiding ((!), HashMap)
import Data.Aeson
import Data.Yaml (decodeEither)
import Data.Version (showVersion)
import Data.Char (toLower, isDigit, digitToInt)
import System.Directory (listDirectory, doesDirectoryExist, doesFileExist, makeAbsolute)
import qualified System.FSNotify as N
import Control.Concurrent (threadDelay)
import Control.Monad (forever, unless)
import Control.Applicative ((<$>))
import System.Exit (exitSuccess)

import qualified Resources as R
import Options
import Types
import Paths_cell (version)

{- Text Utilities -}


infixl 5 ~+~
(~+~) :: Text -> Text -> Text
(~+~) = T.append

safeT :: (Text -> Text) -> Text -> Text
safeT function text
  | T.null text = ""
  | otherwise   = function text

safeHead :: Text -> Text
safeHead = safeT (T.singleton . T.head)

safeTail :: Text -> Text
safeTail = safeT T.tail

safeInit :: Text -> Text
safeInit = safeT T.init


{- Inititalizers -}


readCellDefs :: IO CellDefMap
readCellDefs = do
  cj <- R.cssJson
  case eitherDecodeStrict' cj :: Either String (HashMap Text CellDef) of
    Left  err  -> error $ "Error decoding cell's internal definitions (css.json). The error was: " ++ err
    Right defs -> return defs

readConfig :: FilePath -> IO Config
readConfig fp = do
  configFile <- BS.readFile fp
  let eConfig = decodeEither configFile :: Either String Config
  case eConfig of
    Left  err  -> error $ "Error decoding user config `" ++ fp ++ "`. The error was: " ++ err
    Right conf -> return conf

initCell :: IO Cell
initCell = do
  d <- readCellDefs
  configExists <- doesFileExist "cell.yaml"
  if configExists
    then do
      c <- readConfig "cell.yaml"
      return $ Cell d (HM.fromList pseudoClasses) c
    else return $ Cell d (HM.fromList pseudoClasses) $
      Config Nothing Nothing Nothing Nothing Nothing Nothing


{- CSS pseudo-classes

These values are hardcoded because they are unlikely
to change often and do not need to be modified
by users. The list is converted to a hashmap 
when the Cell record is filled.

-}


pseudoClasses :: [(Text, Text)]
pseudoClasses =
  [ (":a",":active")
  , (":c",":checked")
  , (":d",":default")
  , (":di",":disabled")
  , (":e",":empty")
  , (":en",":enabled")
  , (":fi",":first")
  , (":fc",":first-child")
  , (":fot",":first-of-type")
  , (":fs",":fullscreen")
  , (":f",":focus")
  , (":h",":hover")
  , (":ind",":indeterminate")
  , (":ir",":in-range")
  , (":inv",":invalid")
  , (":lc",":last-child")
  , (":lot",":last-of-type")
  , (":l",":left")
  , (":li",":link")
  , (":oc",":only-child")
  , (":oot",":only-of-type")
  , (":o",":optional")
  , (":oor",":out-of-range")
  , (":ro",":read-only")
  , (":rw",":read-write")
  , (":req",":required")
  , (":r",":right")
  , (":rt",":root")
  , (":s",":scope")
  , (":t",":target")
  , (":va",":valid")
  , (":vi",":visited") ]



{- Get markup files -}


-- Get all filepaths from a path.
getFilePaths :: FilePath -> IO [FilePath]
getFilePaths path = do
  dirExists <- doesDirectoryExist path
  if dirExists
    then do
      dir <- listDirectory path
      let fullpaths = ((path ++ "/") ++) <$> filter ((/= '.') . head) dir
      L.concat <$> mapM getFilePaths fullpaths
    else return [path]

getFileType :: FilePath -> String
getFileType = reverse . (takeWhile (/= '.')) . reverse . (fmap toLower)

-- Predicate for markup files.
isMarkup :: String -> Bool
isMarkup fp =
  case fp of
    "html" -> True
    "htm"  -> True
    "tpl"  -> True
    "mustache" -> True
    _ -> False

-- Predicate for markup & script files.
isScript :: String -> Bool
isScript fp =
  case fp of
    "js"   -> True
    "json" -> True
    _ -> False

-- Get all markup files in a path.
getMarkupFiles :: Config -> FilePath -> IO [Text]
getMarkupFiles conf path = do
  filepaths <- getFilePaths path
  let p = case (_scripts conf) of
        (Just True) -> (\s -> isMarkup s || isScript s) . getFileType
        _ -> isMarkup . getFileType
  case filter p filepaths of
    [] -> exitSuccess
    fs -> mapM T.readFile $ filter p fs


{- Prepare markup files -}


-- Prepare file for sieveing.
prepareFile :: Text -> [Text]
prepareFile file = T.words $ T.map space file
  where
    space '\'' = ' '
    space '"'  = ' '
    space '<'  = ' '
    space '>'  = ' '
    space '='  = ' '
    space '|'  = ' '
    space char = char

-- Sieve for cell classes.
sieveCell :: CellDefMap -> [Text] -> [Text]
sieveCell defmap = filter isCell
  where
    isCell c = HM.member (fst $ T.breakOn "(" c) defmap


{- Make CSS class name from HTML class identifier -}


-- Escape illeagal CSS characters.
escapeCSS :: Text -> Text
escapeCSS = T.concatMap escape
  where
    escape '(' = "\\("
    escape ')' = "\\)"
    escape ',' = "\\,"
    escape '%' = "\\%"
    escape '#' = "\\#"
    escape '`' = "\\`"
    escape '!' = "\\!"
    escape ':' = "\\:"
    escape '/' = "\\/"
    escape '$' = "\\$"
    escape '.' = "\\."
    escape '@' = "\\@"
    escape chr = T.singleton chr

-- Expand pseudo class abbreviation if one exists.
expandPseudoClass :: TextMap -> Text -> Text
expandPseudoClass pseudoMap ident = T.append ident $ HM.lookupDefault "" abbr pseudoMap
  where
    abbr = T.takeWhile (/='-') $ T.dropWhile (/=':') ident

-- Convert HTML class identifier to valid CSS class.
makeClassName :: Cell -> Text -> Text
makeClassName cell ident = T.cons '.' $ expandPseudoClass (_pseudoClassDefs cell) $ escapeCSS ident


{- Process arguments -}


{-
-- Split HTML class identifier into a list of Text arguments, retaining inner parentheses.
argumentSplit :: Text -> [Text]
argumentSplit ident
  | T.isInfixOf "(" arg = parenJoin $ T.splitOn "," arg
  | otherwise = T.splitOn "," arg
  where
    arg = safeTail.safeInit.T.dropWhile (/='(') $ T.dropWhileEnd (/=')') ident

-- Join paren wrapped segments.
parenJoin :: [Text] -> [Text]
parenJoin (h:b:t) =
  | T.isInfixOf "(" h -> parenJoin
  | T.isInfixOf ")" b -> parenJoin
  | otherwise -> h + $ parenJoin $ b ++ t
-}

-- Split HTML class identifier into a list of Text arguments, retaining inner parentheses.
-- This function is the closest thing to pure evil that a haskell function can be.
argumentSplit :: Text -> [Text]
argumentSplit ident
  | T.isInfixOf "(" arg = parenSafeSplit arg
  | otherwise = T.splitOn "," arg
  where
    arg = safeTail.safeInit.T.dropWhile (/='(') $ T.dropWhileEnd (/=')') ident
    recurse p = fst p : argumentSplit (safeTail $ snd p)
    parenSafeSplit s
      | T.isInfixOf "(" (fst $ T.breakOn "," s) = recurse $ (\p -> (fst p ~+~ ")",safeTail $ snd p)) $ T.breakOn ")" s
      | otherwise = recurse $ T.breakOn "," s


{- Process templates -}


-- Get the value of the character that is the largest Int from a Text.
maxCharInt :: Text -> Int
maxCharInt text = fst $ T.mapAccumL count 0 text
  where
    count a c
      | isDigit c && (a < digitToInt c) = (digitToInt c,c)
      | otherwise = (a,c)

{- Substitute replacement-syntax with arguments.
e.g args=[solid,1px,#000] template="border: $0,$1,$2" -> "border: solid,1px,#000" -}
substitute :: [Text] -> Int -> Text -> Text
substitute [] _ t = t
substitute arguments count template
  | count > 0 = substitute arguments (count - 1) $ T.replace target argument template
  | otherwise = T.replace target argument template
  where
    target = " $" ~+~ T.pack (show count)
    argument
     | count >= L.length arguments = ""
     | otherwise = T.cons ' ' $ arguments !! count

-- Fill template by substituting cell arguments.
fillTemplate :: Text -> [Text] -> Text
fillTemplate template arguments = T.stripEnd $ substitute arguments (maxCharInt template) template


{- Prefixing -}


prefixProperty :: (Text,Text) -> Maybe (HashMap Text [Text]) -> Text
prefixProperty _ Nothing            = ""
prefixProperty (p,v) (Just cssrule) = case HM.lookup p cssrule of
  Nothing -> ""
  Just props -> T.concat $ fmap (\a -> a ~+~ v ~+~ "; ") props

prefixValue :: (Text,Text) -> Maybe (HashMap Text [Text]) -> Text
prefixValue _ Nothing            = ""
prefixValue (p,v) (Just cssrule) = case HM.lookup value cssrule of
  Nothing -> ""
  Just vals -> T.concat $ fmap (\a -> p ~+~ ": " ~+~ a ~+~ "; ") vals
  where
    value = T.dropWhile (\z -> z == ':' || z == ' ') v

applyPrefixes :: Config -> Text -> Text
applyPrefixes config preclass = T.concat $ fmap prefixes $ T.splitOn ";" $ safeInit preclass
  where
    prefixes c = prefixProperty parts (_prefixProperties config) ~+~ prefixValue parts (_prefixValues config) ~+~ c ~+~ ";"
      where
        parts = T.breakOn ":" c


{- Properties -}


-- Generate the CSS properties from the HTML identifier.
makeCSSProperties :: Config -> CellDef -> Text -> Text
makeCSSProperties config (CellDef template ali) ident =
  applyPrefixes config $ fillTemplate template $
    expandArgs (argumentSplit ident) $ makeExps ali $ _varibles config

makeExps :: Maybe TextMap -> Maybe TextMap -> TextMap
makeExps Nothing Nothing = HM.empty
makeExps Nothing (Just varsMap) = varsMap
makeExps (Just aliMap) Nothing = aliMap
makeExps (Just aliMap) (Just varsMap) = HM.union aliMap varsMap

expandArgs :: [Text] -> TextMap -> [Text]
expandArgs args exps = fmap expand args
  where
    expand x = HM.lookupDefault x x exps


{- Media Queries -}


-- Apply media query to generated CSS class.
applyMediaQuery :: Config -> Text -> Text -> Text
applyMediaQuery config ident cssclass = let bp = getBreakPoint ident in
  case makeMediaQuery bp $ _breakpoints config of
    Nothing  -> cssclass
    Just mq  -> mq ~+~ cssclass ~+~ "\n}"

-- Make media query Text.
makeMediaQuery :: Text -> Maybe TextMap -> Maybe Text
makeMediaQuery _ Nothing = Nothing
makeMediaQuery bp (Just bpm) = case HM.lookup bp bpm of
  Nothing -> Nothing
  Just val -> Just $ "@media all and (min-width: " ~+~ val ~+~ ") {\n"

-- Get media breakpoint if one exists, else empty Text.
getBreakPoint :: Text -> Text
getBreakPoint = safeTail.T.dropWhile (/='@').snd.T.breakOnEnd ")"


{- Classes -}


makeCSS :: Cell -> CellDefMap -> Text -> Text
makeCSS cell defmap ident = applyMediaQuery config ident $ className ~+~ classBody
  where
    abbr = fst $ T.breakOn "(" ident
    config = _config cell
    className = makeClassName cell ident
    classBody = " {" ~+~ properties ~+~ "}"
      where
        properties
          | T.isInfixOf "!" ident = T.replace ";" " !important;" $ makeCSSProperties config (defmap ! abbr) ident
          | otherwise = makeCSSProperties config (defmap ! abbr) ident

processFile :: Cell -> CellDefMap -> [Text] -> [Text]
processFile cell defmap file = makeCSS cell defmap <$> sieveCell defmap file

processFiles :: Cell -> [Text] -> Text
processFiles cell files = T.unlines $ helpers ++ ["/* ---| Cells |--- */"] ++ standard
  where
    score l = "_" /= safeHead (safeTail l)
    prepared = fmap prepareFile files
    standard = L.sort $ L.nub $ concatMap (processFile cell $ _cellDefs cell) prepared
    helpers = case _helperDefs $ _config cell of
      Nothing -> ["/* ---| No helpers defined in cell.yaml |--- */"]
      Just hr -> do
        let h = L.sortOn score $ L.sort $ L.nub $ concatMap (processFile cell hr) prepared
        if not (null h)
          then "/* ---| Helpers |--- */" : h
          else ["/* ---| No helpers used |--- */"]

process :: Cell -> FilePath -> IO Text
process cell path = do
  markups <- getMarkupFiles (_config cell) path
  return $ processFiles cell markups

main :: IO ()
main = do
  opts <- getOpts
  whenFlag Help opts $ do
    putStrLn
      "Cell CSS Generator, Help\n\
      \Usage: \n\
      \  cell <file|path> <options> > output.css\n\
      \  e.g `cell markupfolder/ > static/style.css`\n\
      \  or, `cell index.html > index.css`\n\
      \  or, `cell -w style.css templates/`\n\
      \  or, `cell --watch=style.css templates/`\n\
      \    to watch templates for changes.\n\
      \Options:\n\
      \  -v -V,        --version         Print version, exit.\n\
      \  -h,           --help            Print this help, exit.\n\
      \  -w <output>,  --watch <output>  Watch directory, output to specified file."
    exitSuccess
  whenFlag Version opts $ do
    putStrLn $ "Cell CSS Generator, Version " ++ showVersion version
    exitSuccess
  cell <- initCell
  let infp = getArg 0 "." opts
  whenKey Watch opts $ \reloutfp -> do
    outfp <- makeAbsolute reloutfp
    manager <- N.startManager
    _ <- N.watchTree manager infp (const True) $ \e -> 
      unless (outfp == N.eventPath e || L.isSuffixOf "~" (N.eventPath e)) $ do
        putStrLn $ "\nChange detected, regenerating `" ++ reloutfp ++ "`"
        process cell infp >>= T.writeFile outfp
    putStrLn "Watching directory.."
    forever $ threadDelay 1000000
  css <- process cell infp
  T.putStr css

